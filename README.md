# umenu16.xyz
uMenu16's webpage.

Frameworks used:
- [ModestaCSS](https://github.com/AlexFlipnote/ModestaCSS)
- [smoothScroll](https://github.com/alicelieutier/smoothScroll)

Big thanks to [AlexFlipnote](https://github.com/AlexFlipnote/alexflipnote.github.io) for his ModestaCSS an for his webpage template!
